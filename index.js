//Outputting a single object

var someObject = { str: "Some text", id: 5 };
console.log(someObject);

// The output looks something like this:
// [09:27:13.475] ({str:"Some text", id:5})

// Outputting multiple objects

var car = "Dodge Charger";
var someObject = { str: "Some text", id: 5 };
console.info("My first car was a", car, ". The object is:", someObject);

// This output will look like this:
// [09:28:22.711] My first car was a Dodge Charger . The object is: ({str:"Some text", id:5})

// Using string substitutions

for (var i=0; i<5; i++) {
    console.log("Hello, %s. You've called me %d times.", "Bob", i+1);
  }

// The output looks like this:
// [13:14:13.481] Hello, Bob. You've called me 1 times.
// [13:14:13.483] Hello, Bob. You've called me 2 times.
// [13:14:13.485] Hello, Bob. You've called me 3 times.
// [13:14:13.487] Hello, Bob. You've called me 4 times.
// [13:14:13.488] Hello, Bob. You've called me 5 times.
  
// Styling console output

console.log("This is %cMy stylish message", "color: yellow; font-style: italic; background-color: blue;padding: 2px");

// There are different types of properties supported for this too.

// Using groups in the console

console.log("This is the outer level");
console.group("First group");
console.log("In the first group");
console.group("Second group");
console.log("In the second group");
console.warn("Still in the second group");
console.groupEnd();
console.log("Back to the first group");
console.groupEnd();
console.debug("Back to the outer level");

// Timers

console.time("answer time");
alert("Click to continue");
console.timeLog("answer time");
alert("Do a bunch of other stuff...");
console.timeEnd("answer time");


// Stack traces

function foo() {
    function bar() {
      console.trace();
    }
    bar();
  }
  
  foo();

  
  console.assert()
//   Log a message and stack trace to console if the first argument is false.
  
  console.clear()
//   Clear the console.
  
  console.count()
//   Log the number of times this line has been called with the given label.
  
  console.countReset()
//   Resets the value of the counter with the given label.
  
  console.debug()
//   Outputs a message to the console with the log level debug.
  
  console.dir()
//   Displays an interactive listing of the properties of a specified JavaScript object. This listing lets you use disclosure triangles to examine the contents of child objects.
  
  console.dirxml()
//   Displays an XML/HTML Element representation of the specified object if possible or the JavaScript Object view if it is not possible.
  
  console.error()
//   Outputs an error message. You may use string substitution and additional arguments with this method.
  
  console.exception()  
//   An alias for error().
  
  console.group()
//   Creates a new inline group, indenting all following output by another level. To move back out a level, call groupEnd().
  
  console.groupCollapsed()
//   Creates a new inline group, indenting all following output by another level. However, unlike group() this starts with the inline group collapsed requiring the use of a disclosure button to expand it. To move back out a level, call groupEnd().
  
  console.groupEnd()
//   Exits the current inline group.
  
  console.info()
//   Informative logging of information. You may use string substitution and additional arguments with this method.
  
  console.log()
//   For general output of logging information. You may use string substitution and additional arguments with this method.
  
  console.profile() 
//   Starts the browser's built-in profiler (for example, the Firefox performance tool). You can specify an optional name for the profile.
  
  console.profileEnd() 
//   Stops the profiler. You can see the resulting profile in the browser's performance tool (for example, the Firefox performance tool).
  
  console.table()
//   Displays tabular data as a table.
  
  console.time()
//   Starts a timer with a name specified as an input parameter. Up to 10,000 simultaneous timers can run on a given page.
  
  console.timeEnd()
//   Stops the specified timer and logs the elapsed time in milliseconds since it started.
  
  console.timeLog()
//   Logs the value of the specified timer to the console.
  
  console.timeStamp() 
//   Adds a marker to the browser's Timeline or Waterfall tool.
  
  console.trace()
//   Outputs a stack trace.
  
  console.warn()
//   Outputs a warning message. You may use string substitution and additional arguments with this method.